import { createApp } from 'vue'
import { MotionPlugin } from '@vueuse/motion'
import VueSmoothScroll from 'vue3-smooth-scroll'
import Vue3Autocounter from 'vue3-autocounter';
import App from './App.vue'

import './assets/main.css'

const app = createApp(App)
app.use(MotionPlugin)
app.use(VueSmoothScroll)
app.use(Vue3Autocounter)
app.mount('#app')
